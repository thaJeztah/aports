# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=evince
pkgver=3.38.2
pkgrel=0
pkgdesc="simple document viewer for GTK+"
url="https://wiki.gnome.org/Apps/Evince"
arch="all !s390x !mips !mips64" # Limited by adwaita-icon-theme needing librsvg
license="GPL-2.0-or-later"
depends="adwaita-icon-theme gsettings-desktop-schemas"
depends_dev="gtk+3.0-dev poppler-dev libsm-dev libevent-dev libxrandr-dev
	libx11-dev libxcursor-dev libxcomposite-dev libxi-dev util-linux-dev
	tiff-dev gobject-introspection-dev libxml2-dev libspectre-dev"
makedepends="$depends_dev itstool meson libexecinfo-dev nautilus-dev
	adwaita-icon-theme-dev gtk-doc yelp-tools appstream-glib-dev
	libgxps-dev gspell-dev gstreamer-dev libsecret-dev gst-plugins-base-dev
	djvulibre-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-nautilus $pkgname-libs"
source="https://download.gnome.org/sources/evince/${pkgver%.*}/evince-$pkgver.tar.xz"

# secfixes:
#   3.32.0-r1:
#     - CVE-2019-11459
#   3.24.0-r2:
#     - CVE-2017-1000083

build() {
	abuild-meson \
		-Dsystemduserunitdir=no \
		-Dgtk_doc=false \
		output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

nautilus() {
	pkgdesc="$pkgname (Nautilus extension)"
	install_if="$pkgname=$pkgver-r$pkgrel nautilus"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/nautilus "$subpkgdir"/usr/lib
}

libs() {
	default_libs
	mv "$pkgdir"/usr/lib/* "$subpkgdir"/usr/lib/
}

doc() {
	default_doc
	if [ -d "$pkgdir"/usr/share/help ]; then
		mv "$pkgdir"/usr/share/help "$subpkgdir"/usr/share/
	fi
}

sha512sums="528e830acd76a458226f0a285b89345558648ccf79ad9f9c426f695c0ce042fe74118d7d924ef9ac91f7c978374c8bdc027b2bd3b1f04ec3ea1db25ad3a3a5b9  evince-3.38.2.tar.xz"
