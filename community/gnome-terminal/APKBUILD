# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-terminal
pkgver=3.38.3
pkgrel=0
pkgdesc="GNOME terminal emulator application"
url="https://wiki.gnome.org/Apps/Terminal"
arch="all !s390x !armhf !ppc64le !mips !mips64" # limited by gnome-shell
license="GPL-2.0-or-later AND GFDL-1.3-only"
depends="dbus desktop-file-utils gsettings-desktop-schemas ncurses-terminfo-base"
makedepends="dconf-dev gnome-shell gsettings-desktop-schemas-dev nautilus-dev vte3-dev
	glib-dev intltool itstool pcre2-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.gnome.org/sources/gnome-terminal/${pkgver%.*}/gnome-terminal-$pkgver.tar.xz
	fix-W_EXITCODE.patch"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="1dc418faaafce019be75173ad035f2536bfec7fea179ecf76f054a62651137339a018198cd6a0cfd63a7a15a1dcf803b62d19a48545a06a133fde186bfbf88ca  gnome-terminal-3.38.3.tar.xz
21a426f7237a07057b83114282fe302787659ec4a171e894abb3542842403bfedbc051b7b19b48866266aeabaaa8d4590fdec0f058b5255b3309f315ae2f1fa6  fix-W_EXITCODE.patch"
